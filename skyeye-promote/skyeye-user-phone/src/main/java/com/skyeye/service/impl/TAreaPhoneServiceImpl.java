/*******************************************************************************
 * Copyright 卫志强 QQ：598748873@qq.com Inc. All rights reserved. 开源地址：https://gitee.com/doc_wei01/skyeye
 ******************************************************************************/

package com.skyeye.service.impl;

import cn.hutool.json.JSONUtil;
import com.app.wechat.util.PhoneConstants;
import com.skyeye.common.object.InputObject;
import com.skyeye.common.object.OutputObject;
import com.skyeye.common.util.ToolUtil;
import com.skyeye.dao.TAreaPhoneDao;
import com.skyeye.jedis.JedisClientService;
import com.skyeye.service.TAreaPhoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class TAreaPhoneServiceImpl implements TAreaPhoneService {

    @Autowired
    private TAreaPhoneDao tAreaPhoneDao;

    @Autowired
    private JedisClientService jedisClient;

    /**
     * 手机端查询省市区数据
     *
     * @param inputObject  入参以及用户信息等获取对象
     * @param outputObject 出参以及提示信息的返回值对象
     */
    @Override
    public void queryTAreaPhoneList(InputObject inputObject, OutputObject outputObject) {
        Map<String, Object> map = inputObject.getParams();
        List<Map<String, Object>> beans = null;
        if (ToolUtil.isBlank(jedisClient.get(PhoneConstants.SYS_ALL_T_AREA_LIST))) {
            beans = tAreaPhoneDao.queryTAreaPhoneList(map);
            beans = ToolUtil.listToTree(beans, "codeId", "parentCodeId", "children");
            jedisClient.set(PhoneConstants.SYS_ALL_T_AREA_LIST, JSONUtil.toJsonStr(beans));
        } else {
            beans = JSONUtil.toList(jedisClient.get(PhoneConstants.SYS_ALL_T_AREA_LIST), null);
        }
        outputObject.setBeans(beans);
        outputObject.settotal(beans.size());
    }

}
